# Pipeline

Description here

## Installation

The recommended installation method is Bower.

### Install using Bower:

    $ bower install --save bully-pipeline

Once installed, `@import` into your project in its Objects layer:

    @import "bower_components/bully-pipeline/components.pipeline";
